package org.sesame.uni.DAO;

import org.sesame.uni.entities.Departement;
import org.sesame.uni.entities.Etudiant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;

@CrossOrigin("*")
@RepositoryRestResource
public interface EtudiantRepository extends JpaRepository<Etudiant,Long> {
    @RestResource(path = "/byName")
    public List<Etudiant> findByNameETUContaining(@Param("mc") String rech);
}
